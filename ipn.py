# This script sends a POST resembling an IPN from PayPal

import argparse
import requests
from data import ipn_data

TRANSACTION_TYPES = ipn_data.keys()

def main(args):
    if args.host == 'localhost':
        localhost = 'http://%s:%s' % (args.host, args.port)
    else:
        localhost = 'http://%s' % args.host
    handler = localhost + args.handler

    # check if app is running...
    try:
        r = requests.get(localhost)
    except requests.exceptions.ConnectionError, e:
        print 'Your web app at %s isn\'t running!' % localhost
        return

    # send IPN...
    print 'Sending POST request to %s...' % handler
    r = requests.post(handler, data=ipn_data[args.txn_type])
    if r.status_code == 200:
        print 'Done!'
    elif r.status_code == 404:
        print 'Oops! %s was not found.' % handler
    else:
        print 'Uh oh! Something went wrong. Check your app\'s logs. '

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Simulates an IPN POST request. By default, sends to http://localhost:8082/ipn.')
    parser.add_argument('txn_type', choices=TRANSACTION_TYPES,
                        help='The IPN transaction type. Possible values: %(choices)s')
    parser.add_argument('-p', '--port', default='8080', help='port number (default=%(default)s)')
    parser.add_argument('-d', '--domain', default='demo1.wemakeprojects.com', help='User\'s Google Apps domain (default=%(default)s)')
    parser.add_argument('-l', '--level', default=1, type=int, choices=[1,2,3], help='Feature level. Possible values: %(choices)s (default=%(default)s)')
    parser.add_argument('--handler', default='/ipn', help='Relative path to IPN handler (default=%(default)s)')
    parser.add_argument('--host', default='localhost', help='Your app\'s host address (default=%(default)s)')
    args = parser.parse_args()
    main(args)

