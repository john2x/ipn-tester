A simple script to test your PayPal IPN handlers locally. 

Run `$ python ipn.py --help` for usage instructions.

Read my [blog post][] on creating IPN handlers for more info.

[blog post]: http://john2x.com/blog/creating-paypal-ipn-handlers-with-tornado/#testing
